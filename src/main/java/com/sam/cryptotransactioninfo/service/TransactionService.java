package com.sam.cryptotransactioninfo.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sam.cryptotransactioninfo.model.TransactionDetail;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TransactionService {

    private final ObjectMapper mapper = new ObjectMapper();


    public TransactionDetail call(String hashId) {
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.exchange("https://apilist.tronscan.org/api/transaction-info?hash=" + hashId, HttpMethod.GET, null, String.class).getBody();

        try {
            TransactionDetail transactionDetail = mapper.readValue(result, TransactionDetail.class);
            return transactionDetail;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;

    }
}
