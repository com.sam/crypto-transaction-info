package com.sam.cryptotransactioninfo.controller;

import com.sam.cryptotransactioninfo.model.TransactionDetail;
import com.sam.cryptotransactioninfo.service.TransactionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/transaction")
public class TransactionController {


    private TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }


    @GetMapping("/{hashId}")
    public ResponseEntity<?> getInfoTransaction(@PathVariable("hashId") String hashId) {

        TransactionDetail result = transactionService.call(hashId);

        return ResponseEntity.ok(result);

    }

}
