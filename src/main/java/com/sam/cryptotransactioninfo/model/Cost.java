package com.sam.cryptotransactioninfo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Cost {

   private Integer multi_sign_fee;
   private Integer net_fee;
   private Integer net_fee_cost;
   private Integer energy_usage;
   private Integer energy_fee_cost;
   private Integer fee;
   private Integer energy_fee;
   private Integer energy_usage_total;
   private Integer memoFee;
   private Integer origin_energy_usage;
   private Integer net_usage;
}
