package com.sam.cryptotransactioninfo.model;

//@author SeyedMohammadi 3/12/2023 1:56 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Trc20TransferInfo {
    private String icon_url;
    private String symbol;
    private String level;
    private String to_address;
    private String contract_address;
    private String type;
    private Integer decimals;
    private String name;
    private Boolean vip;
    private String tokenType;
    private String from_address;
    private String amount_str;
    private Integer status;
}
