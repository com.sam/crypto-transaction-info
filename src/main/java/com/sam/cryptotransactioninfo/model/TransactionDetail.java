package com.sam.cryptotransactioninfo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TransactionDetail {
    private Integer block;
    private String hash;
    private String timestamp;
    private String ownerAddress;
    private ArrayList<Object> signature_addresses;
    private Integer contractType;
    private String toAddress;
    private String project;
    private Integer confirmations;
    private Boolean confirmed;
    private Boolean revert;
    private String contractRet;
    private ContractData contractData;
    private Cost cost;
    private String data;
    private TriggerInfo trigger_info;
    private Object internal_transactions;
    private Integer fee_limit;
    private List<SrConfirm> srConfirmList;
    private String contract_type;
    private Integer event_count;
    private List<TransfersAll> transfersAllList;
    private Info info;
    private AddressTag addressTag;
    private Map<String,Object> contractInfo;
    private Map<String,Boolean> contract_map;
    private TokenTransferInfo tokenTransferInfo;
    public ArrayList<Trc20TransferInfo> trc20TransferInfo;
    public Integer triggerContractType;


}
