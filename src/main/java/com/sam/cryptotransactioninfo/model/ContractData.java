package com.sam.cryptotransactioninfo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ContractData {
    private String data;
    private String owner_address;
    private String contract_address;
    private Integer call_value;
}
