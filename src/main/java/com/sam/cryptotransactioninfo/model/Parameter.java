package com.sam.cryptotransactioninfo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Parameter {
    private String _number;
    private String _direction;
    private String _value;
    private String _to;
}
