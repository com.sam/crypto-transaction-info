package com.sam.cryptotransactioninfo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TransfersAll {
    private String icon_url;
    private String symbol;
    private String level;
    private String to_address;
    private String type;
    private String contract_address;
    private String token_id;
    private Integer decimals;
    private String name;
    private Boolean vip;
    private String tokenType;
    private String amount_str;
    private String from_address;
    private Integer status;
}
