package com.sam.cryptotransactioninfo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TriggerInfo {
    private String method;
    private Parameter parameter;
    private String methodId;
    private String contract_address;
    private Integer call_value;
}
