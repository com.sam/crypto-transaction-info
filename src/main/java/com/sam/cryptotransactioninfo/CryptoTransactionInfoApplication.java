package com.sam.cryptotransactioninfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CryptoTransactionInfoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CryptoTransactionInfoApplication.class, args);
    }

}
